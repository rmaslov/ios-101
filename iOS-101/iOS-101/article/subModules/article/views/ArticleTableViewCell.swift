//
//  ArticleTableViewCell.swift
//  iOS-101
//
//  Created by Rostislav Maslov on 18.01.2018.
//  Copyright © 2018 Rostislav Maslov. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

    static let nibName = "ArticleTableViewCell"
    
    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var bodyLable: UILabel!
    
    func customize(article: ArticleModel){
        titleLable.text = article.title
        bodyLable.text = article.body
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
