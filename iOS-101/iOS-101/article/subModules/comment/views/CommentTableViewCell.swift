//
//  CommentTableViewCell.swift
//  iOS-101
//
//  Created by Rostislav Maslov on 18.01.2018.
//  Copyright © 2018 Rostislav Maslov. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var commentLabel: UILabel!
    
    static let nibName = "CommentTableViewCell"
    
    func customize(comment: CommentModel){
        nameLabel.text = comment.name
        commentLabel.text = comment.body
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
