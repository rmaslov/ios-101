//
//  CommentService.swift
//  iOS-101
//
//  Created by Rostislav Maslov on 27.01.2018.
//  Copyright © 2018 Rostislav Maslov. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON
import Alamofire

typealias CommentsCompletion = (_ comments: [CommentModel]?, _ error: String?) -> Void


class CommentService: TypicodeApiService {
    
    func comments(postId: Int, completion: @escaping CommentsCompletion){
        let url = host + "/comments"
        
        var params:[String : AnyObject] = [:]
        params["postId"] = postId as AnyObject
        
        self.sendRequestWithJSONResponse(
            requestType: .get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (responseData, error) in
                if error != nil {
                    completion(nil, error!.localizedDescription)
                    return
                }else if let comments = responseData!.arrayObject {
                    let commentsModel = Mapper<CommentModel>().mapArray(JSONObject: comments)
                    completion(commentsModel, nil)
                    return
                }
                
                completion(nil, "Ошибка загрузки комментриев")
        }
    }
    
}
