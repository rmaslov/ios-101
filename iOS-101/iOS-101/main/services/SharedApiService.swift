//
//  SharedApiService.swift
//  iOS-101
//
//  Created by Rostislav Maslov on 27.01.2018.
//  Copyright © 2018 Rostislav Maslov. All rights reserved.
//

import Foundation

class SharedApiService: NSObject {
    static let sharedInstance: SharedApiService = { SharedApiService() }()
    
    private(set) var articleService: ArticleService
    private(set) var commentService: CommentService
    
    private override init() {
        self.articleService = ArticleService()
        self.commentService = CommentService()
    }
}
